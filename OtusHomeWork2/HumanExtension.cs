﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusHomeWork2
{
    public static class HumanExtension
    {
        public static int CountChilds(this Human human)
        {    
            return human.Childrens.Count;
        }
    }
}
