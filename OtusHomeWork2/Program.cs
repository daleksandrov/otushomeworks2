﻿using System;

namespace OtusHomeWork2
{
    class Program
    {
        public static void Main(string[] args)
        {
            Human man = new Human("Ваня", true);
            Human woman = new Human("Надя", false);

            //Используем переопределенный оператор "+"
            Human child1 = man + woman;
            child1.SetName("Дима");
            child1.SetGender(true);

            Human child2 = man + woman;                
            child2.SetName("Маша");
            child2.SetGender(false);

            //Используем индексатор
            Console.WriteLine(man[0].ToString());
            Console.WriteLine(man[1].ToString());

            //Используем расширение класса Human
            Console.WriteLine(woman.CountChilds());
            
        }        
    }
}
