﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusHomeWork2
{
    /// <summary>
    /// Класс Человек
    /// </summary>
    public class Human
    {
        // Имя
        private string Name;

        // Пол (true - мужчина, false - женщина)
        private bool Gender;

        // папа
        private Human Father;

        // мама
        private Human Mather;

        public List<Human> Childrens;

        public Human() { Childrens = new List<Human>(); }

        public Human(string name, bool gender) 
        {
            this.Name = name;
            this.Gender = gender;
            Childrens = new List<Human>();
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public void SetGender(bool gender)
        {
            this.Gender = gender;
        }

        private string GetGenderString()
        {
            return this.Gender ? "мальчик" : "девочка";
        }

        /// <summary>
        /// Переопредление оператора "+"
        /// Создание нового человечка
        /// </summary>
        /// <param name="man"></param>
        /// <param name="woman"></param>
        /// <returns></returns>
        public static Human operator +(Human man, Human woman)
        {
            Human child = new Human();
            child.Father = man;
            child.Mather = woman;          
            

            man.Childrens.Add(child);
            woman.Childrens.Add(child);
            
            return child;
        }        

        /// <summary>
        /// Определение индексатора. Только на чтение
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Human this[int index]
        {
            get
            {
                return Childrens[index];
            }
        }

        public override string ToString()
        {
            if (Childrens.Count == 0 && Father != null && Mather != null)
                return $"Имя: {this.Name}; Пол: {GetGenderString()}; Папа: {this.Father?.Name}; Мама: {this.Mather?.Name};";
            else
                return $"Имя: {this.Name}; Пол: {GetGenderString()};";
        }
    }
}
